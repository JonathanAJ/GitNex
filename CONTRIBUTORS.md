# Contributors
This part lists all PUBLIC individuals having contributed content to the code.

 * M M Arif (mmarif)
 * 6543

# Translators
This part lists all PUBLIC individuals having contributed content to the translation.  
*Entries are in alphabetical order*

 * 6543
 * acrylicpaintboy
 * Antoine GIRARD (sapk)
 * BaRaN6161_TURK
 * ButterflyOfFire (BoFFire)
 * dadosch
 * erardiflorian
 * IndeedNotJames
 * jaqra
 * ljoonal
 * Lunny Xiao (xiaolunwen)
 * lxs
 * mmarif
 * Nadezhda Moiseeva (digitalkiller)
 * PsychotherapistSam
 * Rodion Borisov (vintproykt)
 * valeriezhao1013
 * Voyvode

**Thank you for all your work** :+1:
