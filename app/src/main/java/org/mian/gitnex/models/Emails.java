package org.mian.gitnex.models;

/**
 * Author M M Arif
 */

public class Emails {

    private String email;
    private Boolean verified;
    private Boolean primary;

    public String getEmail() {
        return email;
    }

    public Boolean getVerified() {
        return verified;
    }

    public Boolean getPrimary() {
        return primary;
    }
}
